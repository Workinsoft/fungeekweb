import users from './users';
import posts from './posts';

const Sequelize = require('sequelize');

export default function services() {
  const app = this;
  const sequelize = new Sequelize('postgres://postgres:davr0808@localhost:5432/fungeek', {
    dialect: 'postgres',
    logging: false
  });


  app.set('sequelize', sequelize);
  app.configure(users);
  app.configure(posts);
}
