import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { isLoaded as isInfoLoaded, load as loadInfo } from 'redux/modules/info';
import {
  isLoaded as isAuthLoaded,
  load as loadAuth,
  logout
} from 'redux/modules/auth';
import { push } from 'react-router-redux';
import { asyncConnect } from 'redux-connect';
import { Menu, Icon } from 'antd';

@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];

      if (!isAuthLoaded(getState())) {
        promises.push(dispatch(loadAuth()));
      }
      if (!isInfoLoaded(getState())) {
        promises.push(dispatch(loadInfo()));
      }
      return Promise.all(promises);
    }
  }
])
@connect(
  state => ({
    notifs: state.notifs,
    user: state.auth.user
  }),
  { logout, pushState: push }
)
export default class App extends Component {
  render() {
    return (
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1">
          <Link to="/admin">
            <Icon type="home" />
            <span>Home</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/admin/posts">
            <Icon type="file-text" />
            <span>Посты</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/admin/social">
            <Icon type="share-alt" />
            <span>Социальные сети</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/admin/settings">
            <Icon type="setting" />
            <span>Настройки сайта</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="5">
          <Link to="/admin/users">
            <Icon type="user" />
            <span>Пользователи</span>
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}
