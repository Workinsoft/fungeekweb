import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isLoaded, load } from 'redux/modules/posts';
import { push } from 'react-router-redux';
import { asyncConnect } from 'redux-connect';
import { Table, Modal } from 'antd';
import { DropOption } from 'components';

const confirm = Modal.confirm;

@asyncConnect([
  {
    promise: ({ store: { dispatch, getState } }) => {
      const promises = [];

      if (!isLoaded(getState())) {
        promises.push(dispatch(load()));
      }

      return Promise.all(promises);
    }
  }
])
@connect(
  state => ({
    posts: state.posts
  }),
  { pushState: push }
)
export default class PostsList extends Component {
  static propTypes = {
    posts: PropTypes.object.isRequired
  };

  static defaultProps = {
    user: null
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  state = {
    pagination: {}
  };

 handleMenuClick = (record, e) => {
   if (e.key === '1') {
     // onEditItem(record);
     console.log('editing');
   } else if (e.key === '2') {
     confirm({
       title: 'Вы уверены, что хотите удалить запись?',
       onOk() {
         console.log('remove');
         // onDeleteItem(record.id);
       },
     });
   }
 }

 render() {
   const { posts } = this.props;
   console.log(posts);
   const columns = [
     {
       title: 'Заголовок(ru)',
       dataIndex: 'name_ru'
     },
     {
       title: 'Заголовок(en)',
       dataIndex: 'name_en'
     },
     {
       title: 'Видео(ru)',
       dataIndex: 'video_ru'
     },
     {
       title: 'Видео(en)',
       dataIndex: 'video_en'
     },
     {
       title: 'Дата создания',
       dataIndex: 'createdAt'
     },
     {
       title: 'Просмотров',
       dataIndex: 'views'
     },
     {
       title: 'Нравится',
       dataIndex: 'likes'
     },
     {
       title: 'Действия',
       dataIndex: 'operations',
       render: (text, record) =>
         (<DropOption
           onMenuClick={e => this.handleMenuClick(record, e)}
           menuOptions={[{ key: '1', name: 'Редактировать' }, { key: '2', name: 'Удалить' }]}
         />)
     }
   ];
   return (
     <Table
       columns={columns}
			 bordered
       rowKey={record => record.id}
       dataSource={posts.items}
       pagination={this.state.pagination}
       loading={posts.loading}
       onChange={this.handleTableChange}
     />
   );
 }
}
